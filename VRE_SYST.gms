*-------------------------------------------------------------------------------
*-Definitions-------------------------------------------------------------------

Sets
  h                  "hours"
  hs(h)              "hours of fish spawn season"
  ;

Parameters
* -- Economic parameters -------------------------------------------------------
  FC_wind            "fixed annual cost of wind, kEUR/MW"
  FC_solar           "fixed annual cost of solar, kEUR/MW"
  FC_bio             "fixed annual cost of biomass, kEUR/MW"
  VC_bio             "variable cost of biomass, kEur/MWh"
  FC_P2H             "fixed annual costs of alkaline electrolyser, kEUR/MW"
  FC_H2store         "fixed annual costs of P2P storage capacity, kEUR/MWh"
  VC_H2store         "variable costs of using P2P storage capacity, kEUR/MWh"
  FC_H2P             "fixed annual costs of hydrogen-to-power system, kEur/MW"
  NFutureVal_E       "normalized future value of stored energy, kEUR/MWh"
  FC_hydro           "fixed annual costs of hydro (HPS, HE) systems, kEur"
  VC_hydro           "variable cost of using PHS capacity, kEur/MWh"
* -- Technical system parameters -----------------------------------------------
* Hydro technologies
  Ef_PHS_ch          "PHS    charge efficiency"
  Ef_PHS_dch         "PHS discharge efficiency"
  P_PHS_ch           "PHS charge efficiency"
  P_PHS_dch          "PHS power production capacity, MW"
  P_HE               "HE power capacity, MW"
  OutFlow_min        "minimum river outflow, 1000m3/h"
  WED_lower          "water energy density of the lower reservoir (taking into account HE genertor efficiency), MWh/1000m3"
  WED_upper          "water energy density of the upper reservoir, MWh/1000m3"
  V_lower_max        "volume of lower water reservoir, 1000 m3"
  V_upper_max        "volume of upper water reservoir, 1000 m3"
  dV_24h             "allowed daily water volume chage in HE water reservoir, 1000m3"
  dV_24hs            "allowed daily water volume chage in HE water reservoir during fish reproduction season, 1000m3"
* Storage technologies
  Ef_P2H             "power-to-hydrogen system efficiency"
  Ef_H2store_ch      "hydrogen storage    charge efficiency (compresion)  "
  Ef_H2store_dch     "hydrogen storage discharge efficiency (purification)"
  Ef_H2P             "hydrogen-to-power system efficiency"
* -- Refrence year parameters (all values are of that year) --------------------
  GridLosses         "grid losses as % of final demand"
  P_solar_ref        "installed wind power generation capacity, MW"
  P_wind_ref         "installed wind power generation capacity, MW"
  Share_wind_ref     "wind share in power production"
  Share_solar_ref    "solar share in power production"
  E_bio_ref          "electicital energy produced, MWh"
  FinalConsump(h)    "hourly energy consumption, MW"
  Gen_wind_ref(h)    "wind  power geneatione, MW"
  Gen_solar_ref(h)   "solar power geneation, MW"
  InFlow(h)          "water inflow into HE reservoir, 1000m3/h"
  ;

Positive variables
* Economic variables
  Cost_production    "annual costs of all power production technologies, kEUR"
  Cost_wind          "annual costs of wind, kEUR"
  Cost_solar         "annual costs of solar, kEUR"
  Cost_bio           "annual costs of biomass, kEUR"
  Cost_hydro         "annual costs of hydro, kEUR"
  Cost_P2P           "annual costs of P2P system, kEUR"
  Cost_P2H           "annual costs of power-to-hydrogen system, kEUR"
  Cost_H2store       "annual costs of hydrogen stroage, kEUR"
  Cost_H2P           "annual costs of hydrogen-to-power system, kEUR"
  FutureVal_E        "future value of stored energy, kEUR"
* Production variables
  Share_wind           "wind share in power production"
  Share_solar          "solar share in power production"
  P_wind               "installed wind power generation capacity, MW"
  P_solar              "installed wind solar generation capacity, MW"
  P_bio                "installed wind biomass generation capacity, MW"
  E_bio                "annual energy production by biomass, MWh"
  Gen_wind(h)          "wind power generation, MW"
  Gen_wind_curtail(h)  "curtailed wind power generation, MW"
  Gen_solar(h)         "solar power generation, MW"
  Gen_solar_curtail(h) "curtailed solar power generation, MW"
  Gen_bio(h)           "biomass power generation, MW"
* Hydro variables
  Gen_HE(h)         "hydro power generation, MW"
  Gen_PHS_dch(h)    "pumped hydro generation, MW"
  Gen_PHS_ch(h)     "pumped hydro charging electricity use, MW"
  Spin(h)           "water flow from HE reservoir used to spin the HE turbines"
  Spill(h)          "water flow from HE reservoir without spinning the turbines"
  Pump_up(h)        "water flow from lower reservoir to upper one"
  Pump_down(h)      "water flow from upper reservoir to lower one"
  V_lower(h)        "volume of water in the lower reservoir, 1000m3"
  V_lower_start     "volume of water in the lower reservoir at the start of simulaion, 1000m3"
  V_upper(h)        "volume of water in the upper reservoir, 1000m3"
  V_upper_start     "volume of water in the upper reservoir at the start of simulaion, 1000m3"
* Storage variables
  E_H2store(h)      "energy stored in form of hydrogen, MWh"
  E_H2store_start   "energy stored in form of hydrogen at the start of simulation, MWh"
  E_H2store_max     "hydrogen storage capacity, MWh"
  P_P2H             "power-to-hydrogen conversion capacity, MW"
  P_H2P             "hydrogen-to-power conversion capacity, MW"
  Gen_P2H           "hydrogen production rate, MW"
  Gen_H2P           "power production using hydrogen, MW"
  ;

Free variable
  SystCost    ;

*-------------------------------------------------------------------------------
*-Reading-----------------------------------------------------------------------

$gdxin MatLab2Gams.gdx
$load FC_wind, FC_solar, FC_bio, FC_hydro, FC_H2P, FC_H2store, FC_P2H
$load VC_bio, VC_H2store, VC_hydro, NFutureVal_E
$load Ef_H2store_ch, Ef_H2store_dch, Ef_H2P, Ef_P2H, Ef_PHS_ch, Ef_PHS_dch
$load P_HE, P_PHS_ch, P_PHS_dch
$load V_lower_max, V_upper_max,  WED_lower, WED_upper, dV_24h, dV_24hs, OutFlow_min
$load GridLosses, P_wind_ref, P_solar_ref, Share_wind_ref, Share_solar_ref, E_bio_ref
$load h, hs, FinalConsump, Gen_wind_ref, Gen_solar_ref, InFlow
$gdxin

*-------------------------------------------------------------------------------
*-Optimisation------------------------------------------------------------------

Equations
  ObjectF           "objective function"
  Cost_P0
  Cost_P1
  Cost_P2
  Cost_P3
  Cost_H
  Cost_S0
  Cost_S1
  Cost_S2
  Cost_S3
  Cost
  ScaleUp1
  ScaleUp2
  ScaleUp3(h)
  ScaleUp4(h)
  Balance(h)
  Balance_P
  Balance_H1(h)
  Balance_H2(h)
  Balance_H3(h)
  Balance_H4(h)
  Balance_H5(h)
  Balance_S(h)
  Limit_P1(h)
  Limit_P2(h)
  Limit_P3
  Limit_P4(h)
  Limit_H1
  Limit_H2
  Limit_H3(h)
  Limit_H4(h)
  Limit_H5(h)
  Limit_H6(h)
  Limit_H7(hs)
  Limit_H8(hs)
  Limit_H9(h)
  Limit_H10(h)
  Limit_H11(h)
  Limit_H12(h)
  Limit_H13(h)
  Limit_S1
  Limit_S2(h)
  Limit_S3(h)
  Limit_S4(h)
  ;

ObjectF       .. SystCost          =e= Cost_production + Cost_hydro + Cost_P2P + FutureVal_E ;

Cost_P0       .. Cost_production   =e= Cost_wind + Cost_solar + Cost_bio ;
Cost_P1       .. Cost_wind         =e= P_wind * FC_wind ;
Cost_P2       .. Cost_solar        =e= P_solar * FC_solar ;
Cost_P3       .. Cost_bio          =e= P_bio * FC_bio + E_bio * VC_bio ;
Cost_H        .. Cost_hydro        =e= FC_hydro + sum(h, Gen_PHS_ch(h)) * VC_hydro ;
Cost_S0       .. Cost_P2P          =e= Cost_P2H + Cost_H2store + Cost_H2P ;
Cost_S1       .. Cost_P2H          =e= P_P2H * FC_P2H ;
Cost_S2       .. Cost_H2store      =e= E_H2store_max * FC_H2store + sum(h, Gen_P2H(h)) * VC_H2store ;
Cost_S3       .. Cost_H2P          =e= P_H2P * FC_H2P ;
Cost          .. FutureVal_E       =e= ( E_H2store('8760') - E_H2store('1') +
                                         ( V_upper('8760') - V_upper('1') ) * WED_upper +
                                         ( V_lower('8760') - V_lower('1') ) * WED_lower  ) *
                                       NFutureVal_E ;

ScaleUp1      .. P_wind            =e= P_wind_ref       * Share_wind  / Share_wind_ref  ;
ScaleUp2      .. P_solar           =e= P_solar_ref      * Share_solar / Share_solar_ref ;
ScaleUp3(h)   .. Gen_wind(h)       =e= Gen_wind_ref(h)  * Share_wind  / Share_wind_ref  ;
ScaleUp4(h)   .. Gen_solar(h)      =e= Gen_solar_ref(h) * Share_solar / Share_solar_ref ;

Balance(h)    .. FinalConsump(h) *
                 (1 + GridLosses)  =e= Gen_wind(h) - Gen_wind_curtail(h) + Gen_solar(h) - Gen_solar_curtail(h) +
                                       Gen_HE(h) + Gen_bio(h) +
                                       Gen_PHS_dch(h) - Gen_PHS_ch(h) +
                                       Gen_H2P(h)     - Gen_P2H(h)     ;
Balance_P     .. E_bio             =e= sum(h, Gen_bio(h));
Balance_H1(h) .. V_lower(h)        =e= V_lower_start$(ord(h) = 1) + V_lower(h-1) + InFlow(h) - Spin(h) - Spill(h) - Pump_up(h) + Pump_down(h);
Balance_H2(h) .. Gen_HE(h)         =e= WED_lower * Spin(h) ;
Balance_H3(h) .. V_upper(h)        =e= V_lower_start$(ord(h) = 1) + V_upper(h-1) + Pump_up(h) - Pump_down(h);
Balance_H4(h) .. Gen_PHS_ch(h)     =e= WED_upper * Pump_up(h) / Ef_PHS_ch ;
Balance_H5(h) .. Gen_PHS_dch(h)    =e= WED_upper * Pump_down(h) * Ef_PHS_dch ;
Balance_S(h)  .. E_H2store(h)      =e= E_H2store_start$(ord(h) = 1) + E_H2store(h-1) +
                                       Gen_P2H(h) * (Ef_P2H * Ef_H2store_ch  )  -
                                       Gen_H2P(h) / (Ef_H2P * Ef_H2store_dch )   ;

Limit_P1(h)   .. Gen_wind_curtail(h)  =l= Gen_wind(h)  ;
Limit_P2(h)   .. Gen_solar_curtail(h) =l= Gen_solar(h) ;
Limit_P3      .. E_bio                =l= E_bio_ref ;
Limit_P4(h)   .. Gen_bio(h)           =l= P_bio ;

Limit_H1      .. V_lower_start        =l= V_lower_max ;
Limit_H2      .. V_upper_start        =l= V_upper_max ;
Limit_H3(h)   .. V_lower(h)           =l= V_lower_max ;
Limit_H4(h)   .. V_upper(h)           =l= V_upper_max ;
Limit_H5(h)   .. V_lower(h+24)  - V_lower(h)  =l= dV_24h ;
Limit_H6(h)   .. V_lower(h-24)  - V_lower(h)  =l= dV_24h ;
Limit_H7(hs)  .. V_lower(hs+24) - V_lower(hs) =l= dV_24hs ;
Limit_H8(hs)  .. V_lower(hs-24) - V_lower(hs) =l= dV_24hs ;
Limit_H9(h)   .. Spin(h)              =g= OutFlow_min ;
Limit_H10(h)  .. Spill(h)             =l= InFlow(h) ;
Limit_H11(h)  .. Gen_HE(h)            =l= P_HE  ;
Limit_H12(h)  .. Gen_PHS_ch(h)        =l= P_PHS_ch ;
Limit_H13(h)  .. Gen_PHS_dch(h)       =l= P_PHS_dch ;

Limit_S1      .. E_H2store_start      =l= E_H2store_max ;
Limit_S2(h)   .. E_H2store(h)         =l= E_H2store_max ;
Limit_S3(h)   .. Gen_P2H(h)           =l= P_P2H ;
Limit_S4(h)   .. Gen_H2P(h)           =l= P_H2P ;

Model vre_syst /all/ ;
Solve vre_syst using LP minimizing SystCost ;


