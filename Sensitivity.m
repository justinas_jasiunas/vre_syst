clear all;
clc;

load('Scenarios.mat');

reference = Close8;
scenario  = reference;

for d = [1 2 3 4 5]
    
    scenario.FC_solar.val = reference.FC_solar.val / d;
    [cost, size] = ScenarioRun(scenario);
    costMatrix(d, :) = cost;
    sizeMatrix(d, :) = size;
    
end

scenario.FC_wind.val = reference.FC_wind.val / 2;
for d = [1 2 3 4 5]
    
    scenario.FC_solar.val = reference.FC_solar.val / d;
    [cost, size] = ScenarioRun(scenario);
    costMatrix(5+d, :) = cost;
    sizeMatrix(5+d, :) = size;
    
end

scenario.FC_solar.val = reference.FC_solar.val;
for d = [3 4 5]
    
    scenario.FC_wind.val = reference.FC_wind.val / d;
    [cost, size] = ScenarioRun(scenario);
    costMatrix(8+d, :) = cost;
    sizeMatrix(8+d, :) = size;
    
end

scenario.FC_solar.val = reference.FC_solar.val / 2;
for d = [3 4 5]
    
    scenario.FC_wind.val = reference.FC_wind.val / d;
    [cost, size] = ScenarioRun(scenario);
    costMatrix(11+d, :) = cost;
    sizeMatrix(11+d, :) = size;
    
end