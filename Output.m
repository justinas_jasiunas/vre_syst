function Out = Output()

    Out.h                 = gdxValueRead('h')                            ;
    Out.FinalConsump      = gdxFullVectorRead(Out.h, 'FinalConsump')     ;
    Out.Gen_wind          = gdxFullVectorRead(Out.h, 'Gen_wind')         ;
    Out.Gen_wind_curtail  = gdxFullVectorRead(Out.h, 'Gen_wind_curtail') ;
    Out.Gen_solar         = gdxFullVectorRead(Out.h, 'Gen_solar')        ;
    Out.Gen_solar_curtail = gdxFullVectorRead(Out.h, 'Gen_solar_curtail');
    Out.Gen_bio           = gdxFullVectorRead(Out.h, 'Gen_bio')          ;
    Out.Gen_HE            = gdxFullVectorRead(Out.h, 'Gen_HE')           ;
    Out.V_lower           = gdxFullVectorRead(Out.h, 'V_lower')          ;
    Out.InFlow            = gdxFullVectorRead(Out.h, 'InFlow')           ;
    Out.Spin              = gdxFullVectorRead(Out.h, 'Spin')             ;
    Out.Spill             = gdxFullVectorRead(Out.h, 'Spill')            ;
    Out.dV24              = difVector(Out.V_lower)                       ;
    Out.V_upper           = gdxFullVectorRead(Out.h, 'V_upper')          ;
    Out.Pump_up           = gdxFullVectorRead(Out.h, 'Pump_up')          ;
    Out.Pump_down         = gdxFullVectorRead(Out.h, 'Pump_down')        ;
    Out.Gen_PHS_ch        = gdxFullVectorRead(Out.h, 'Gen_PHS_ch')       ;
    Out.Gen_PHS_dch       = gdxFullVectorRead(Out.h, 'Gen_PHS_dch')      ;
    Out.Gen_P2H           = gdxFullVectorRead(Out.h, 'Gen_P2H')          ;
    Out.E_H2store         = gdxFullVectorRead(Out.h, 'E_H2store')        ;
    Out.Gen_H2P           = gdxFullVectorRead(Out.h, 'Gen_H2P')          ;

    Out.Share_wind        = gdxValueRead('Share_wind')   ;
    Out.Share_solar       = gdxValueRead('Share_solar')  ;
    Out.P_wind            = gdxValueRead('P_wind')       ;
    Out.P_solar           = gdxValueRead('P_solar')      ;
    Out.P_bio             = gdxValueRead('P_bio')        ;
    Out.P_P2H             = gdxValueRead('P_P2H')        ;
    Out.E_H2store_max     = gdxValueRead('E_H2store_max');
    Out.P_H2P             = gdxValueRead('P_H2P')        ;
    Out.Cost_wind         = gdxValueRead('Cost_wind')    ;
    Out.Cost_solar        = gdxValueRead('Cost_solar')   ;
    Out.Cost_bio          = gdxValueRead('Cost_bio')     ;
    Out.Cost_hydro        = gdxValueRead('Cost_hydro')   ;
    Out.Cost_P2H          = gdxValueRead('Cost_P2H')     ;
    Out.Cost_H2store      = gdxValueRead('Cost_H2store') ;
    Out.Cost_H2P          = gdxValueRead('Cost_H2P')     ;
    Out.SystCost          = gdxValueRead('SystCost')     ;

end

function value = gdxValueRead (name)
    
    x.name = name;
    y = rgdx('VRE_SYST', x);
    value = y.val;
    
end

function fullVector = gdxFullVectorRead (h, name)

    sparceVector = gdxValueRead(name);
    L            = length(h);
    fullVector   = zeros(L, 1);
    for i = sparceVector(:,1)
        fullVector(i) = sparceVector(:,2);
    end
    
end

function dV24 = difVector (fullVector)

    fullVector24 = [fullVector(end-23:end); fullVector(1:end-24)];
    dV24 = fullVector24 - fullVector;
    
end