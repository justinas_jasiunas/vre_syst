function  [cost, size] = ScenarioRun(scenario)

    Input(scenario);
    system 'gams VRE_SYST.gms gdx=VRE_SYST'
    gamsOut = Output();

    cost = CostComposition(gamsOut, scenario);
    size = SystemSize(gamsOut);

end

function matCost = CostComposition(gamsOut, Scenario)

    cost.Bio     = gamsOut.Cost_bio;
    cost.Hydro   = gamsOut.Cost_hydro;
    cost.P2H     = gamsOut.Cost_P2H;
    cost.H2store = gamsOut.Cost_H2store;
    cost.H2P     = gamsOut.Cost_H2P;   

    VRE_production = sum(gamsOut.Gen_wind) + sum(gamsOut.Gen_solar);
    VRE_curtailed  = sum(gamsOut.Gen_wind_curtail) + sum(gamsOut.Gen_solar_curtail);
    curtailShare   = VRE_curtailed / VRE_production;
    cost.Wind_Curtailed  = gamsOut.Cost_wind  * curtailShare;
    cost.Solar_Curtailed = gamsOut.Cost_solar * curtailShare;
    
    P2P_charge      = sum(gamsOut.Gen_P2H);
    P2P_discharge   = sum(gamsOut.Gen_H2P);
    P2P_levelChange = gamsOut.E_H2store(end) - gamsOut.E_H2store(1);
    P2P_losses      = P2P_charge - P2P_discharge - P2P_levelChange * Scenario.Ef_H2P.val * Scenario.Ef_H2store_dch.val;
    PHS_levelChange = gamsOut.V_upper(end) - gamsOut.V_upper(1);
    PHS_charge      = sum(gamsOut.Gen_PHS_ch);
    PHS_discharge   = sum(gamsOut.Gen_PHS_dch);
    PHS_losses      = PHS_charge - PHS_discharge - PHS_levelChange * Scenario.WED_upper.val * Scenario.Ef_PHS_dch.val;
    lossesShare     = (P2P_losses + PHS_losses) / VRE_production;
    cost.Wind_StorageLosses  = gamsOut.Cost_wind  * lossesShare;
    cost.Solar_StorageLosses = gamsOut.Cost_solar * lossesShare;    
    
    cost.Wind_FinalConsumption  = gamsOut.Cost_wind  * (1 - curtailShare - lossesShare);
    cost.Solar_FinalConsumption = gamsOut.Cost_solar * (1 - curtailShare - lossesShare);
    
    matCost = struct2mat(cost);
    
end

function matSize = SystemSize(gamsOut)

    size.P_wind  = gamsOut.P_wind;
    size.P_solar = gamsOut.P_solar;
    size.P2H     = gamsOut.P_P2H;
    size.H2P     = gamsOut.P_H2P;
    size.H2store = gamsOut.E_H2store_max;
    size.bio     = gamsOut.P_bio;
    
    matSize = struct2mat(size);
    
end

function mat = struct2mat(struct)

    cellArray = struct2cell(struct);
    emptyElements = cellfun('isempty', cellArray);
    cellArray(emptyElements) = {0};    
    mat = transpose(cell2mat(cellArray));
    
end